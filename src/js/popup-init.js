$(document).ready(function() {
  $(".modal-link-1").magnificPopup({
    items: {
      src: "#popup-1",
      type: "inline"
    }
  });
  $(".modal-link-2").magnificPopup({
    items: {
      src: "#popup-2",
      type: "inline"
    }
  });
  $(".modal-link-3").magnificPopup({
    items: {
      src: "#popup-3",
      type: "inline"
    }
  });
  $(".modal-link-4").magnificPopup({
    items: {
      src: "#popup-4",
      type: "inline"
    }
  });
});
